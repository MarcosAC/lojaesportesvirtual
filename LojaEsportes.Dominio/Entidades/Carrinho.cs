﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LojaEsportes.Dominio.Entidades
{
    public class Carrinho
    {
        private List<CarrinhoItem> itensCarrinho = new List<CarrinhoItem>();

        //adicionar item
        public void AdicionarItem(Produto produto, int quantidade)
        {
            CarrinhoItem item = itensCarrinho.Where(p => p.Produto.IdProduto == produto.IdProduto).FirstOrDefault();

            if (item == null)
            {
                itensCarrinho.Add(new CarrinhoItem
                {
                    Produto = produto,
                    Quantidade = quantidade
                });             
            }
        }

        //remover item
        public void RemoverItem(Produto produto)
        {
            itensCarrinho.RemoveAll(item => item.Produto.IdProduto == produto.IdProduto);
        }

        //calcular o total
        public decimal CalcularValorTotal()
        {
            return itensCarrinho.Sum(p => p.Produto.Preco * p.Quantidade);
        }

        // limpar carrinho
        public void LimparCarrinho()
        {
            itensCarrinho.Clear();
        }

        //Itens
        public IEnumerable<CarrinhoItem> Itens
        {
            get { return itensCarrinho; }
        }
    }
}
