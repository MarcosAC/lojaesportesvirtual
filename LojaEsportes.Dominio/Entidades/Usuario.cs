﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LojaEsportes.Dominio.Entidades
{
    [Table("Usuarios")]
    public class Usuario
    {

        public Usuario()
        {
            UsuarioPermissoes = new HashSet<UsuarioPermissao>();
        }

        [Key]
        public int IdUsuario { get; set; }

        [Required(ErrorMessage = "Informe o Login.")]
        [Display(Name = "Usuário")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Informe o Senha.")]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        public virtual ICollection<UsuarioPermissao> UsuarioPermissoes { get; set; }
    }
}
