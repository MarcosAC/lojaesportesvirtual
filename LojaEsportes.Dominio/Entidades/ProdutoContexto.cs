﻿using System.Data.Entity;

namespace LojaEsportes.Dominio.Entidades
{
    public class ProdutoContexto : DbContext
    {
        public ProdutoContexto()
            : base("name=ConexaoEsportes")
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<ProdutoContexto>(new CreateDatabaseIfNotExists<ProdutoContexto>());
        }

        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<Permissao> Permissoes { get; set; }
        public virtual DbSet<UsuarioPermissao> UsuarioPermissoes { get; set; }
    }
}
