﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace LojaEsportes.Dominio.Entidades
{
    public class EnviarEmailPedido : IProcessarPedido
    {
        private EmailConfiguracao emailConfiguracao;

        public EnviarEmailPedido()
        {
            emailConfiguracao = new EmailConfiguracao();
        }
        public void ProcessarPedido(Carrinho carrinho, DetalhesPedido detalhesPedido)
        {
            using (var smtpClient = new SmtpClient())
            {
                smtpClient.EnableSsl = emailConfiguracao.UsarSsl;
                smtpClient.Host = emailConfiguracao.Servidor;
                smtpClient.Port = emailConfiguracao.ServidorPorta;
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new NetworkCredential(emailConfiguracao.Usuario, emailConfiguracao.Senha);
                emailConfiguracao.EmailDestino = detalhesPedido.Email;

                if (emailConfiguracao.EscreverComoArquivo)
                {
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    smtpClient.PickupDirectoryLocation = emailConfiguracao.Arquivo;
                    smtpClient.EnableSsl = false;
                }

                StringBuilder body = new StringBuilder()
                    .AppendLine("Um novo pedido foi enviado")
                    .AppendLine("---")
                    .AppendLine("Itens : ");

                foreach (var item in carrinho.Itens)
                {
                    var subtotal = item.Produto.Preco * item.Quantidade;
                    body.AppendFormat("{0} x {1} (sub-total: {2:c}", item.Quantidade, item.Produto.Nome, subtotal);
                }

                body.AppendFormat("Valor Total do Pedido : {0:c}", carrinho.CalcularValorTotal())
                    .AppendLine("---")
                    .AppendLine("Enviar Para:")
                    .AppendLine(detalhesPedido.Nome)
                    .AppendLine(detalhesPedido.Endereco)
                    .AppendLine(detalhesPedido.Complemento ?? "")
                    .AppendLine(detalhesPedido.Cep)
                    .AppendLine(detalhesPedido.Cidade ?? "")
                    .AppendLine(detalhesPedido.Estado)
                    .AppendLine(detalhesPedido.Email)
                    .AppendLine("---")
                    .AppendFormat("Pacote Presente : {0}", detalhesPedido.PacotePresente ? "Sim" : "Nao");

                MailMessage mailMessage = new MailMessage(
                                             emailConfiguracao.EmailOrigem,   //De
                                             emailConfiguracao.EmailDestino,  //Para
                                             "Novo Pedido Enviado!",          //Assunto
                                             body.ToString());                //Texto

                if (emailConfiguracao.EscreverComoArquivo)
                {
                    mailMessage.BodyEncoding = Encoding.ASCII;
                }
                try
                {
                    smtpClient.Send(mailMessage);
                }
                catch (Exception ex)
                {
                    throw ex.InnerException;
                }
            }//fim using
        }
    }
}
