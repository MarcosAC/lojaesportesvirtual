﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LojaEsportes.Dominio.Entidades
{
    [Table("Permissoes")]
    public class Permissao
    {
        public Permissao()
        {
            UsuarioPermissoes = new HashSet<UsuarioPermissao>();
        }

        [Key]
        public int IdPermissao { get; set; }

        [Required(ErrorMessage = "Informe a Permissão.")]
        [Display(Name = "Permissão")]
        public string Nome { get; set; }

        public virtual ICollection<UsuarioPermissao> UsuarioPermissoes { get; set; }
    }
}
