﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LojaEsportes.Dominio.Entidades
{
    [Table("Produtos")]
    public class Produto
    {
        [Key]
        public int IdProduto { get; set; }

        [Required(ErrorMessage = "Informe o nome do produto.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Informe a descrição do produto.")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        [Required(ErrorMessage = "Informe o preço do produto")]

        [Display(Name = "Preço")]
        public decimal Preco { get; set; }
        public byte[] Imagem { get; set; }
        public string ImagemTipo { get; set; }

        [Display(Name = "Categoria")]
        public int IdCategoria { get; set; }
        public virtual Categoria Categoria { get; set; }
    }
}
