﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LojaEsportes.Dominio.Entidades
{
    [Table("UsuarioPermissoes")]
    public class UsuarioPermissao
    {
        [Key]
        public int IdPermissaoUsuario { get; set; }

        [Display(Name = "Permissão")]
        public int IdPermissao { get; set; }

        [Display(Name = "Usuário")]
        public int IdUsuario { get; set; }

        public virtual Permissao Permissao { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
