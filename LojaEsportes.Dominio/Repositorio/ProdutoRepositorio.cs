﻿using LojaEsportes.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LojaEsportes.Dominio.Repositorio
{
    public class ProdutoRepositorio : IRepositorio<Produto>
    {
        private ProdutoContexto contexto;

        public ProdutoRepositorio(ProdutoContexto produtoContexto)
        {
            contexto = produtoContexto;
        }

        //Retorna todos os produtos.
        public IEnumerable<Produto> Get(int? id)
        {
            return contexto.Produtos.Where(categora => categora.IdCategoria == id).OrderBy(produto => produto.Nome);
        }

        //Retorna produtos por categoria.
        public IEnumerable<Produto> GetAll()
        {
            return contexto.Produtos.ToList().OrderBy(produto => produto.Nome);
        }
    }
}
