﻿using LojaEsportes.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LojaEsportes.Dominio.Repositorio
{
    public class CategoriaRepositorio : IRepositorio<Categoria>
    {
        private ProdutoContexto contexto;

        public CategoriaRepositorio(ProdutoContexto produtoContexto)
        {
            contexto = produtoContexto;
        }

        public IEnumerable<Categoria> Get(int? id)
        {
            return contexto.Categorias.Where(categoria => categoria.IdCategoria == id);
        }

        public IEnumerable<Categoria> GetAll()
        {
            return contexto.Categorias.OrderBy(categoria => categoria.Nome);
        }
    }
}
