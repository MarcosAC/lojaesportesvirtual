﻿using LojaEsportes.Dominio.Entidades;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace LojaEsportes.Web.Controllers
{
    [Authorize]
    public class AdministradorController : Controller
    {
        private ProdutoContexto db = new ProdutoContexto();

        public ActionResult MenuAdministrador()
        {
            return View();
        }

        // GET: Administrador
        public ActionResult Index()
        {
            var produtos = db.Produtos.Include(p => p.Categoria);
            return View(produtos.ToList());
        }

        // GET: Administrador/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produto produto = db.Produtos.Find(id);
            if (produto == null)
            {
                return HttpNotFound();
            }
            return View(produto);
        }

        // GET: Administrador/Create
        public ActionResult Create()
        {
            ViewBag.IdCategoria = new SelectList(db.Categorias, "IdCategoria", "Nome");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdProduto,Nome,Descricao,Preco,Imagem,ImagemTipo,IdCategoria")] Produto produto, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    var arquivoImagem = new Produto
                    {
                        ImagemTipo = upload.ContentType
                    };
                    using (var leitor = new BinaryReader(upload.InputStream))
                    {
                        arquivoImagem.Imagem = leitor.ReadBytes(upload.ContentLength);
                    }
                    produto.Imagem = arquivoImagem.Imagem;
                    produto.ImagemTipo = arquivoImagem.ImagemTipo;
                }
                db.Produtos.Add(produto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCategoria = new SelectList(db.Categorias, "IdCategoria", "Nome", produto.IdCategoria);
            return View(produto);
        }

        // GET: Administrador/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produto produto = db.Produtos.Find(id);
            if (produto == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdCategoria = new SelectList(db.Categorias, "IdCategoria", "Nome", produto.IdCategoria);
            return View(produto);
        }
                
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdProduto,Nome,Descricao,Preco,Imagem,ImagemTipo,IdCategoria")] Produto produto, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    var arquivoImagem = new Produto
                    {
                        ImagemTipo = upload.ContentType
                    };
                    using (var leitor = new BinaryReader(upload.InputStream))
                    {
                        arquivoImagem.Imagem = leitor.ReadBytes(upload.ContentLength);
                    }
                    produto.Imagem = arquivoImagem.Imagem;
                    produto.ImagemTipo = arquivoImagem.ImagemTipo;
                }
                db.Entry(produto).State = EntityState.Modified;
                db.SaveChanges();
                TempData["mensagem"] = string.Format("{0}  : foi atualizado com sucesso", produto.Nome);
                return RedirectToAction("Index");
            }
            ViewBag.IdCategoria = new SelectList(db.Categorias, "CategoriaId", "Nome", produto.IdCategoria);
            return View(produto);
        }

        // GET: Administrador/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produto produto = db.Produtos.Find(id);
            if (produto == null)
            {
                return HttpNotFound();
            }
            return View(produto);
        }

        // POST: Administrador/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Produto produto = db.Produtos.Find(id);
            db.Produtos.Remove(produto);
            db.SaveChanges();
            TempData["mensagem"] = string.Format("{0}  : foi excluído com sucesso", produto.Nome);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
