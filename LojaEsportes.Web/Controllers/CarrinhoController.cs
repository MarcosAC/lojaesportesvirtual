﻿using LojaEsportes.Dominio.Entidades;
using LojaEsportes.Dominio.Repositorio;
using LojaEsportes.Web.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LojaEsportes.Web.Controllers
{
    [HandleError(ExceptionType = typeof(Exception), View = "_Error")]
    public class CarrinhoController : Controller
    {
        private IRepositorio<Produto> produtoRepositorio;
        private IProcessarPedido processarPedido;

        public CarrinhoController()
        {
            produtoRepositorio = new ProdutoRepositorio(new ProdutoContexto());
            processarPedido = new EnviarEmailPedido();
        }

        // GET: Carrinho
        public ActionResult Index(string ReturnUrl)
        {
            return View(new CarrinhoViewModel { Carrinho = GetCarrinho(), ReturnUrl = ReturnUrl });
        }

        public ActionResult ItensCarrinho(Carrinho carrinho)
        {
            carrinho = GetCarrinho();
            return PartialView(carrinho);
        }

        //GET
        public ActionResult FinalizarCompra()
        {
            return View(new DetalhesPedido { Carrinho = GetCarrinho() });
        }

        [HttpPost]
        public ActionResult FinalizarCompra(Carrinho carrinho, DetalhesPedido detalhesPedido)
        {
            carrinho = GetCarrinho();

            if (carrinho.Itens.Count() == 0)
            {
                ModelState.AddModelError("", "Seu carrinho esta vazio !");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    processarPedido.ProcessarPedido(carrinho, detalhesPedido);
                    carrinho.LimparCarrinho();
                    return View("PedidoConcluido");
                }
                catch (Exception ex)
                {
                    return View("_Erro", new HandleErrorInfo(ex, "FinalizarCompra", "Carrinho"));
                }
            }
            else
            {
                return View(detalhesPedido);
            }
        }
        public RedirectToRouteResult AdicionarItemCarrinho(int IdProduto, string ReturnUrl)
        {
            Produto produto = produtoRepositorio.GetAll().FirstOrDefault(p => p.IdProduto == IdProduto);

            if (produto != null)
            {
                GetCarrinho().AdicionarItem(produto, 1);
            }

            return RedirectToAction("Index", new { ReturnUrl });
        }

        public RedirectToRouteResult RemoverItemCarrinho(int IdProduto, string ReturnUrl)
        {
            Produto produto = produtoRepositorio.GetAll().FirstOrDefault(p => p.IdProduto == IdProduto);

            if (produto != null)
            {
                GetCarrinho().RemoverItem(produto);
            }

            return RedirectToAction("Index", new { ReturnUrl });
        }

        private Carrinho GetCarrinho()
        {
            Carrinho carrinho = (Carrinho)Session["Carrinho"];

            if (carrinho == null)
            {
                carrinho = new Carrinho();
                Session["Carrinho"] = carrinho;
            }

            return carrinho;
        }       
    }
}