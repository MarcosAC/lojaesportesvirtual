﻿using LojaEsportes.Dominio.Entidades;
using LojaEsportes.Dominio.Repositorio;
using System.Web.Mvc;

namespace LojaEsportes.Web.Controllers
{
    public class NavegaController : Controller
    {
        private IRepositorio<Categoria> categoriaRepositorio;

        public NavegaController()
        {
            categoriaRepositorio = new CategoriaRepositorio(new ProdutoContexto());
        }

        public ActionResult Menu()
        {
            var categorias = categoriaRepositorio.GetAll();
            return PartialView(categorias);
        }
    }
}