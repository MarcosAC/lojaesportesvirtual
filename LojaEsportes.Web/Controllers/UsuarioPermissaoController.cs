﻿using LojaEsportes.Dominio.Entidades;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace LojaEsportes.Web.Controllers
{
    public class UsuarioPermissaoController : Controller
    {
        private ProdutoContexto db = new ProdutoContexto();

        // GET: UsuarioPermissao
        public ActionResult Index()
        {
            var usuarioPermissoes = db.UsuarioPermissoes.Include(u => u.Permissao).Include(u => u.Usuario);
            return View(usuarioPermissoes.ToList());
        }

        // GET: UsuarioPermissao/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsuarioPermissao usuarioPermissao = db.UsuarioPermissoes.Find(id);
            if (usuarioPermissao == null)
            {
                return HttpNotFound();
            }
            return View(usuarioPermissao);
        }

        // GET: UsuarioPermissao/Create
        public ActionResult Create()
        {
            ViewBag.IdPermissao = new SelectList(db.Permissoes, "IdPermissao", "Nome");
            ViewBag.IdUsuario = new SelectList(db.Usuarios, "IdUsuario", "Login");
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdPermissaoUsuario,IdPermissao,IdUsuario")] UsuarioPermissao usuarioPermissao)
        {
            if (ModelState.IsValid)
            {
                db.UsuarioPermissoes.Add(usuarioPermissao);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdPermissao = new SelectList(db.Permissoes, "IdPermissao", "Nome", usuarioPermissao.IdPermissao);
            ViewBag.IdUsuario = new SelectList(db.Usuarios, "IdUsuario", "Login", usuarioPermissao.IdUsuario);
            return View(usuarioPermissao);
        }

        // GET: UsuarioPermissao/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsuarioPermissao usuarioPermissao = db.UsuarioPermissoes.Find(id);
            if (usuarioPermissao == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdPermissao = new SelectList(db.Permissoes, "IdPermissao", "Nome", usuarioPermissao.IdPermissao);
            ViewBag.IdUsuario = new SelectList(db.Usuarios, "IdUsuario", "Login", usuarioPermissao.IdUsuario);
            return View(usuarioPermissao);
        }
                
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdPermissaoUsuario,IdPermissao,IdUsuario")] UsuarioPermissao usuarioPermissao)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuarioPermissao).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdPermissao = new SelectList(db.Permissoes, "IdPermissao", "Nome", usuarioPermissao.IdPermissao);
            ViewBag.IdUsuario = new SelectList(db.Usuarios, "IdUsuario", "Login", usuarioPermissao.IdUsuario);
            return View(usuarioPermissao);
        }

        // GET: UsuarioPermissao/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsuarioPermissao usuarioPermissao = db.UsuarioPermissoes.Find(id);
            if (usuarioPermissao == null)
            {
                return HttpNotFound();
            }
            return View(usuarioPermissao);
        }

        // POST: UsuarioPermissao/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UsuarioPermissao usuarioPermissao = db.UsuarioPermissoes.Find(id);
            db.UsuarioPermissoes.Remove(usuarioPermissao);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
