﻿using LojaEsportes.Dominio.Entidades;
using LojaEsportes.Dominio.Repositorio;
using PagedList;
using System.Web.Mvc;

namespace LojaEsportes.Web.Controllers
{
    public class ProdutoController : Controller
    {
        private IRepositorio<Produto> repositorioProduto;

        public ProdutoController()
        {
            repositorioProduto = new ProdutoRepositorio(new ProdutoContexto());
        }
        // GET: Produto
        public ActionResult Catalogo(int? pagina, int? categoria)
        {
            if (categoria == null)
            {
                return View(repositorioProduto.GetAll().ToPagedList(pagina ?? 1, 2));
            }
            else
            {
                return View(repositorioProduto.Get(categoria).ToPagedList(pagina ?? 1, 2));
            }
            
        }
    }
}